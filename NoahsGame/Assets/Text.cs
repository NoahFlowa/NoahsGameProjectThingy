﻿using NoahsGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    /// <summary>
    /// class to store static and to generate dynamic text for the message and input boxes
    /// </summary>
    public static class Text
    {
        public static List<string> HeaderText = new List<string>() { "Wastelander" };
        public static List<string> FooterText = new List<string>() { "BluePrintGames, 2018" };

        #region INTITIAL GAME SETUP

        /// <summary>
        /// Missions the intro.
        /// </summary>
        /// <returns>The intro.</returns>
        public static string MissionIntro()
        {
            string messageBoxText =
            "War. War Never Changes. \n" +
            "Since the dawn of humankind, when our ancestors first discovered " +
            "the killing power of rock and bone, blood has been spilled in the " +
            "name of everything: from God to justice to simple, psychotic rage. " +
            "In the year 2088, after millennia of armed conflict, the destructive " +
            "nature of man could sustain itself no longer. The world was plunged into " +
            "an abyss of nuclear fire and radiation.  But it was not, as some had predicted, " +
            "the end of the world. Instead, the apocalypse was simply the prologue to another " +
            "bloody chapter of human history. For man had succeeded in destroying the world - but war, war never changes." +
            " \n" +
            "Press the Esc key to exit the game at any point.\n" +
            " \n" +
            "Your chapter has only begun.\n" +
            " \n" +
            "You were a simple Wastelander, who roamed the wastes of a forgotten America.  You have " +
            "roamed from the West where you were born, all the way to the East where you find yourself " +
            "in the middle of an armed conflict between two super factions that could turn a new page of this " +
            "nuclear burned world into something that resembles the one that was forgotten." +
            " \n" +
            "\tPress any key to begin.\n";

            return messageBoxText;
        }

        /// <summary>
        /// Currrents the location info.
        /// </summary>
        /// <returns>The location info.</returns>
        public static string CurrrentLocationInfo()
        {
            string messageBoxText =
            "You are now in the Norlon Corporation research facility located in " +
            "the city of Heraklion on the north coast of Crete. You have passed through " +
            "heavy security and descended an unknown number of levels to the top secret " +
            "research lab for the Aion Project.\n" +
            " \n" +
            "\tChoose from the menu options to proceed.\n";

            return messageBoxText;
        }

        /// <summary>
        /// Initializes the mission get traveler home location.
        /// </summary>
        /// <returns>The mission get traveler home location.</returns>
        /// <param name="name">Name.</param>
        public static string InitializeMissionGetTravelerHomeLocation(string name)
        {
            string messageBoxText =
            $"{name}, before you started out East, in the West, where did you travel from? ";

            return messageBoxText;
        }

        #region Initialize Mission Text

        /// <summary>
        /// Initializes the mission intro.
        /// </summary>
        /// <returns>The mission intro.</returns>
        public static string InitializeMissionIntro()
        {
            string messageBoxText =
                "Before you begin, you must provide what kind of Wastelander you are...\n" +
                " \n" +
                "You will be prompted for the required information. Please enter the information below.\n" +
                " \n" +
                "\tPress any key to begin.";

            return messageBoxText;
        }

        /// <summary>
        /// Initializes the name of the mission get traveler.
        /// </summary>
        /// <returns>The mission get traveler name.</returns>
        public static string InitializeMissionGetTravelerName()
        {
            string messageBoxText =
                "Enter your name Wastelander.\n" +
                " \n" +
                "Please use the name you wish to be praised/feared by...";

            return messageBoxText;
        }

        /// <summary>
        /// Games the objects choose list.
        /// </summary>
        /// <returns>The objects choose list.</returns>
        /// <param name="gameObjects">Game objects.</param>
        public static string GameObjectsChooseList(IEnumerable<GameObject> gameObjects) {
            string messageBoxText = $"Game Objects\n" +
                "\n" +
                "ID".PadRight(10) +
                "Name".PadRight(30) + "\n" +
                "---".PadRight(10) +
                "---------------------------".PadRight(30) + "\n";

            string gameObjectRows = null;
            foreach (GameObject gameObject in gameObjects)
            {
                gameObjectRows +=
                    $"{gameObject.Id}".PadRight(10) +
                    $"{gameObject.Name}".PadRight(30) +
                    Environment.NewLine;
            }

            messageBoxText += gameObjectRows;
            return messageBoxText;
        }

        /// <summary>
        /// Initializes the mission get traveler age.
        /// </summary>
        /// <returns>The mission get traveler age.</returns>
        /// <param name="name">Name.</param>
        public static string InitializeMissionGetTravelerAge(string name)
        {
            string messageBoxText =
                $"Very good then, you will be known as {name}.\n" +
                " \n" +
                "Enter your age below.\n" +
                " \n";

            return messageBoxText;
        }

        /// <summary>
        /// Initializes the mission get traveler class.
        /// </summary>
        /// <returns>The mission get traveler class.</returns>
        /// <param name="gameTraveler">Game traveler.</param>
        public static string InitializeMissionGetTravelerClass(Traveler gameTraveler)
        {
            string messageBoxText =
                $"{gameTraveler.Name}, it will be important for others to know what kind of Wastelander you are.\n" +
                " \n" +
                "Enter your class below.\n" +
                " \n" +
                "Please use the universal classes classifications below." +
                " \n";

            string raceList = null;

            foreach (Character.ClassType race in Enum.GetValues(typeof(Character.ClassType)))
            {
                if (race != Character.ClassType.None)
                {
                    raceList += $"\t{race}\n";
                }
            }

            messageBoxText += raceList;

            return messageBoxText;
        }

        /// <summary>
        /// Initializes the mission get traveler special.
        /// </summary>
        /// <returns>The mission get traveler special.</returns>
        /// <param name="gameTraveler">Game traveler.</param>
        public static string InitializeMissionGetTravelerSpecial(Traveler gameTraveler)
        {
            string messageBoxText =
                $"{gameTraveler.Name}, it is important you choose a SPECIAL as this will help you in the game.\n" +
                " \n" +
                "Enter your starting SPECIAL below {You will get one everytime you finish a major mission}.\n" +
                " \n" +
                "Please use the universal SPECIAL classifications below." +
                " \n";

            string specialList = null;

            foreach (Traveler.SPECIAL special in Enum.GetValues(typeof(Traveler.SPECIAL)))
            {
                if (special != Traveler.SPECIAL.None)
                {
                    specialList += $"\t{special}\n";
                }
            }

            messageBoxText += specialList;

            return messageBoxText;
        }

        /// <summary>
        /// Initializes the mission echo traveler info.
        /// </summary>
        /// <returns>The mission echo traveler info.</returns>
        /// <param name="gameTraveler">Game traveler.</param>
        public static string InitializeMissionEchoTravelerInfo(Traveler gameTraveler)
        {
            string messageBoxText =
                $"Very good then {gameTraveler.Name}.\n" +
                " \n" +
                "It appears we have all the necessary data to begin your mission. You will find it" +
                " listed below.\n" +
                " \n" +
                $"Wastelander Name: {gameTraveler.Name}\n" +
                $"Wastelander Age: {gameTraveler.Age}\n" +
                $"Wastelander Home: {gameTraveler.HomeLocation}\n" +
                $"Wastelander Race: {gameTraveler.Class}\n" +
                $"Wastelander Starting SPECIAL: {gameTraveler.Special}\n" +
                " \n" +
                "Press any key to begin your mission.";

            return messageBoxText;
        }

        /// <summary>
        /// Displaies the changed traveler info.
        /// </summary>
        /// <returns>The changed traveler info.</returns>
        /// <param name="gameTraveler">Game traveler.</param>
        public static string DisplayChangedTravelerInfo(Traveler gameTraveler)
        {
            string messageBoxText =
                $"{gameTraveler.Name}, here is the following info: \n" +
                " \n" +
                $"Wastelander Name: {gameTraveler.Name}\n" +
                $"Wastelander Age: {gameTraveler.Age}\n" +
                $"Wastelander Race: {gameTraveler.Class}\n" +
                $"Wastelander Starting SPECIAL: {gameTraveler.Special}\n" +
                " \n" +
                "Choose a main menu option to continue.";

            return messageBoxText;
        }

        #endregion

        #endregion

        #region MAIN MENU ACTION SCREENS

        /// <summary>
        /// Travelers the info.
        /// </summary>
        /// <returns>The info.</returns>
        /// <param name="gameTraveler">Game traveler.</param>
        public static string TravelerInfo(Traveler gameTraveler)
        {
            string messageBoxText =
                $"\tWastelander Name: {gameTraveler.Name}\n" +
                $"\tWastelander Age: {gameTraveler.Age}\n" +
                $"\tWastelander Home: {gameTraveler.HomeLocation}\n" +
                $"\tWastelander Class: {gameTraveler.Class}\n" +
                $"\tWastelander SPECIAL: {gameTraveler.Special}\n" +
                $"\tWastelander Greeting: {gameTraveler.FirstContact()}\n" +

                " \n";

            return messageBoxText;
        }

        /// <summary>
        /// Lists the space time locations.
        /// </summary>
        /// <returns>The space time locations.</returns>
        /// <param name="spaceTimeLocations">Space time locations.</param>
        public static string ListSpaceTimeLocations(IEnumerable<SpaceTimeLocation> spaceTimeLocations) {
            string messageBoxText =
                $"Locations: \n" +
                " \n" +
                "ID:".PadRight(10) + "Name".PadRight(30) + "\n" + "----".PadRight(10) + "-------------------------------".PadRight(30) + "\n";

            string spaceTimeLocationList = null;
            foreach (SpaceTimeLocation spaceTimeLocation in spaceTimeLocations)
            {
                spaceTimeLocationList +=
                    $"{spaceTimeLocation.SpaceTimeLocationID}".PadRight(10) + $"{spaceTimeLocation.CommonName}".PadRight(30) + Environment.NewLine;
            }

            messageBoxText += spaceTimeLocationList;

            return messageBoxText;
        }

        /// <summary>
        /// Looks the around.
        /// </summary>
        /// <returns>The around.</returns>
        /// <param name="spaceTimeLocation">Space time location.</param>
        public static string LookAround(SpaceTimeLocation spaceTimeLocation)
        {
            string messageBoxText = $"Current Location: {spaceTimeLocation.CommonName} \n" +
                " \n" +
                spaceTimeLocation.GeneralContents;


            return messageBoxText;
        }

        /// <summary>
        /// Looks at gameObject.
        /// </summary>
        /// <returns>The <see cref="T:System.String"/>.</returns>
        /// <param name="gameObject">Game object.</param>
        public static string LookAt(GameObject gameObject) {
            string messageBoxText = "";

            messageBoxText = $"{gameObject.Name}\n" +
                "\n" +
                gameObject.Description + "\n" +
                "\n";

            if (gameObject is Object)
            {
                Object travelerObject = gameObject as Object;
                messageBoxText += $"The {travelerObject.Name} has a value of {travelerObject.Value} and ";

                if (travelerObject.CanInventory)
                {
                    messageBoxText += "may be added to your inventory.";
                } else {
                    messageBoxText += "may not be added to your inventory";
                }
            }  else {
                messageBoxText += $"The {gameObject.Name} may not be added to your inventory";
            }

            return messageBoxText;
        }

        /// <summary>
        /// Lists all game objects.
        /// </summary>
        /// <returns>The all game objects.</returns>
        /// <param name="gameObjects">Game objects.</param>
        public static string ListAllGameObjects(IEnumerable<GameObject> gameObjects)
        {
            string messageBoxText = "Game Objects \n" +
                "\n" +
                "ID".PadRight(10) +
                "Name".PadRight(30) +
                "Location ID".PadRight(10) + "\n" +
                "---".PadRight(10) +
                "-----------".PadRight(30) + "\n" +
                "-----------".PadRight(10) + "\n";


            string gameObjectRows = null;
            foreach (GameObject gameObject in gameObjects)
            {
                gameObjectRows +=
                    $"{gameObject.Id}".PadRight(10) +
                    $"{gameObject.Name}".PadRight(30) +
                    $"{gameObject.LocationId}".PadRight(10) +
                    Environment.NewLine;
            }

            messageBoxText += gameObjectRows;

            return messageBoxText;

        }

        public static string NPCChooseList(IEnumerable<NPC> npcs)
        {
            string messageBoxText = "NPCs \n" +
               "\n" +
               "ID".PadRight(10) +
               "Name".PadRight(30) +
               "---".PadRight(10) +
               "-----------".PadRight(30) + "\n";


            string npcRows = null;
            foreach (NPC npc in npcs)
            {
                npcRows +=
                    $"{npc.ID}".PadRight(10) +
                    $"{npc.Name}".PadRight(30) +
                    Environment.NewLine;
            }

            messageBoxText += npcRows;

            return messageBoxText;
        }

        public static string CurrentInventory(IEnumerable<GameObject> inventory) {
            string messageBoxText = "";

            messageBoxText = "\n" +
                "ID".PadRight(10) +
                "Name".PadRight(30) +
                "Type".PadRight(10) + "\n" +
                "---".PadRight(10) +
                "-----------".PadRight(30) + "\n" +
                "-----------".PadRight(10) + "\n";

            string inventoryObjectRows = null;
            foreach (Object inventoryObject in inventory)
            {
                inventoryObjectRows += 
                    $"{inventoryObject.Id}".PadRight(10) +
                    $"{inventoryObject.Name}".PadRight(30) +
                    $"{inventoryObject.Type}".PadRight(10) +
                    Environment.NewLine;
            }

            messageBoxText += inventoryObjectRows;

            return messageBoxText;

        }

        /// <summary>
        /// Travel the specified gametraveler and spaceTimeLocations.
        /// </summary>
        /// <returns>The travel.</returns>
        /// <param name="gametraveler">Gametraveler.</param>
        /// <param name="spaceTimeLocations">Space time locations.</param>
        public static string Travel(Traveler gametraveler, List<SpaceTimeLocation> spaceTimeLocations)
        {
            string messageBoxText = $"{gametraveler.Name}, Where do you want to travel next? \n" + 
                " \n" + 
                "Enter the ID number of your desired location from the table below.\n" + 
                " \n " +

            "ID".PadRight(10) + "Name".PadRight(30) + "Accessable".PadRight(10) + "\n" + "----".PadRight(10) + "-------------------------------".PadRight(30) + "---------".PadRight(10) + "\n";

            string spaceTimeLocationList = null;
            foreach (SpaceTimeLocation spaceTimeLocation in spaceTimeLocations)
            {
                if (spaceTimeLocation.SpaceTimeLocationID != gametraveler.SpaceTimeLocationID)
                {
                    spaceTimeLocationList += $"{spaceTimeLocation.SpaceTimeLocationID}".PadRight(10) +
                        $"{spaceTimeLocation.CommonName}".PadRight(30) +
                        $"{spaceTimeLocation.Accessable}".PadRight(10) +
                        Environment.NewLine;
                }
            }

            messageBoxText += spaceTimeLocationList;

            return messageBoxText;
        }

        /// <summary>
        /// Currents the location info.
        /// </summary>
        /// <returns>The location info.</returns>
        /// <param name="spaceTImeLocation">Space TI me location.</param>
        public static string CurrentLocationInfo(SpaceTimeLocation spaceTImeLocation)
        {
            string messageBoxText = $"Current Location: {spaceTImeLocation.CommonName}\n" +
                "\n" +
                spaceTImeLocation.Description;

            return messageBoxText;
        }

        /// <summary>
        /// Visiteds the locations.
        /// </summary>
        /// <returns>The locations.</returns>
        /// <param name="spaceTimeLocations">Space time locations.</param>
        public static string VisitedLocations(IEnumerable<SpaceTimeLocation> spaceTimeLocations)
        {
            string messageBoxText = $"Visited Locations: \n" + 
                "\n" + 
                "ID".PadRight(10) + "Name".PadRight(30) + "\n" +
                "----".PadRight(10) + "------------------------------------------".PadRight(30) + "\n";

            string spaceTimeLocationList = null;
            foreach (SpaceTimeLocation spaceTimeLocation in spaceTimeLocations)
            {
                spaceTimeLocationList += $"{spaceTimeLocation.SpaceTimeLocationID}".PadRight(10) +
                    $"{spaceTimeLocation.CommonName}".PadRight(30) + 
                    Environment.NewLine;
            }


            messageBoxText += spaceTimeLocationList;

            return messageBoxText;
        }

        public static string ListAllNPCObjects(IEnumerable<NPC> npcObjects)
        {
            string messageBoxText = "NPC Objects \n" +
                "\n" +
                "ID".PadRight(10) +
                "Name".PadRight(30) +
                "Location ID".PadRight(10) + "\n" +
                "---".PadRight(10) +
                "-----------".PadRight(30) + "\n" +
                "-----------".PadRight(10) + "\n";


            string npcObjectRows = null;
            foreach (NPC npcObject in npcObjects)
            {
                npcObjectRows +=
                    $"{npcObject.ID}".PadRight(10) +
                    $"{npcObject.Name}".PadRight(30) +
                    $"{npcObject.SpaceTimeLocationID}".PadRight(10) +
                    Environment.NewLine;
            }

            messageBoxText += npcObjectRows;

            return messageBoxText;
        }

        public static string ListAllMerchantWares(IEnumerable<GameObject> civInventoryObjects)
        {
            string messageBoxText = "NPC Inventory \n" +
                "\n" +
                "ID".PadRight(10) +
                "Name".PadRight(30) +
                "---".PadRight(10) +
                "-----------".PadRight(30);


            string civObjectInventoryRows = null;
            foreach (GameObject invObject in civInventoryObjects)
            {
                civObjectInventoryRows +=
                    $"{invObject.Id}".PadRight(10) +
                    $"{invObject.Name}".PadRight(30) +
                    Environment.NewLine;
            }

            messageBoxText += civObjectInventoryRows;

            return messageBoxText;
        }

        public static string NPCsChooseList(IEnumerable<NPC> npcs)
        {
            string messageBoxText = "NPC Objects \n" +
                "\n" +
                "ID".PadRight(10) +
                "Name".PadRight(30) +
                "---".PadRight(10) +
                "-----------".PadRight(30) + "\n";


            string npcRows = null;
            foreach (NPC npc in npcs)
            {
                npcRows +=
                    $"{npc.ID}".PadRight(10) +
                    $"{npc.Name}".PadRight(30) +
                    Environment.NewLine;
            }

            messageBoxText += npcRows;

            return messageBoxText;
        }

        #endregion

        /// <summary>
        /// Statuses the box.
        /// </summary>
        /// <returns>The box.</returns>
        /// <param name="traveler">Traveler.</param>
        public static List<string> StatusBox(Traveler traveler)
        {
            List<string> statusBoxText = new List<string>();

            statusBoxText.Add($"Wastelanders's Age: {traveler.Age}\n");
            statusBoxText.Add($"Experience Points: {traveler.ExperiencePoints}\n");
            statusBoxText.Add($"Wastelanders's Health: {traveler.Health}\n");
            statusBoxText.Add($"Wastelanders's Lives: {traveler.Lives}\n");

            return statusBoxText;
        }
    }
}
