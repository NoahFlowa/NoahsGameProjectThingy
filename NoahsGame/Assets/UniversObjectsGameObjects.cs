﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    public partial class UniversObjectsGameObjects
    {
        public static List<GameObject> gameObjects = new List<GameObject>()
        {
            new Object {
                Id = 1,
                Name = "Stash of Caps",
                LocationId = 1,
                Description = "A small stash hidden that contains Caps",
                PickUpMessage = "You picked up 60 caps!",
                Type = ObjectType.Treasure,
                Value = 60,
                CanInventory = true,
                IsConsumable = true,
                IsVisible = true
            },

            new Object {
                Id = 2,
                Name = "Mountain O' Caps",
                LocationId = 3,
                Description = "A big mountain of Caps that technically shouldn't be a thing, however it is....",
                PickUpMessage = "You picked up 120 caps",
                Type = ObjectType.Treasure,
                Value = 120,
                CanInventory = true,
                IsConsumable = true,
                IsVisible = true
            },

            new Object {
                Id = 3,
                Name = "Stimpak",
                LocationId = 3,
                Description = "A Stimpak to heal your limbs and wounds",
                PickUpMessage = "You picked up a Stimpack, injected it and now you are healed",
                Type = ObjectType.Medicine,
                Value = 60,
                CanInventory = false,
                IsConsumable = true,
                IsVisible = true
            },

            new Object {
                Id = 4,
                Name = "Pre-War Document",
                LocationId = 3,
                Description = "This Pre-War document is valuable to the right person...",
                PickUpMessage = "You picked up a Pre-War document!",
                Type = ObjectType.Information,
                Value = 0,
                CanInventory = true,
                IsConsumable = false,
                IsVisible = true
            },

            new Object {
                Id = 5,
                Name = "Bip-Man",
                LocationId = 0,
                Description = "This pre war device was your fathers and his fathers trusted device.  Ancient and mysterious computer worn around the wrist, this allows you to travel and hold items that no one should be abe to carry an excessive amount of...",
                Type = ObjectType.Information,
                Value = 0,
                CanInventory = true,
                IsConsumable = false,
                IsVisible = true
            },

            new Object {
                Id = 6,
                Name = "Lucky 14 Canteen",
                LocationId = 0,
                Description = "This pre war canteen seems to never run out of water...",
                Type = ObjectType.Information,
                Value = 0,
                CanInventory = true,
                IsConsumable = false,
                IsVisible = true
            },

            new Object {
                Id = 7,
                Name = "M1 Garand",
                LocationId = 0,
                Description = "This ole Carbine as been in more wars and conflicts than you can imagine, yet always the user is sworn by it's build and deadliness",
                Type = ObjectType.Weapon,
                Value = 90,
                CanInventory = true,
                IsConsumable = false,
                IsVisible = true
            },

            new Object {
                Id = 8,
                Name = "AK-47",
                LocationId = 5,
                Description = "The Ruskies favorite weapon.  These damn commies are always known to carry this weapon and much like the M1-A1 Carbine, you can find this everywhere in Eastern Europe",
                Type = ObjectType.Weapon,
                Value = 60,
                CanInventory = true,
                IsConsumable = false,
                IsVisible = true
            },

            new Object {
                Id = 9,
                Name = ".30 Caliber Rounds",
                LocationId = 0,
                Description = "",
                Type = ObjectType.Weapon,
                Value = 60,
                CanInventory = true,
                IsConsumable = true,
                IsVisible = true
            },

            new Object {
                Id = 10,
                Name = "7.62x39mm Rounds",
                LocationId = 0,
                Description = "",
                Type = ObjectType.Weapon,
                Value = 60,
                CanInventory = true,
                IsConsumable = true,
                IsVisible = true
            },
        };
    }
}