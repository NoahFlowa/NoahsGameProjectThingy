﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    /// <summary>
    /// static class to hold all objects in the game universe; locations, game objects, npc's
    /// </summary>
    public static partial class UniverseObjectsLocations
    {
        public static List<SpaceTimeLocation> SpaceTimeLocations = new List<SpaceTimeLocation>()
        {

            new SpaceTimeLocation
            {
                CommonName = "Arroyo",
                SpaceTimeLocationID = 1,
                UniversalDate = 652165,
                UniversalLocation = "Eastern Commonwealth",
                Description = "Arroyo, the birthplace of civilization in the new world.  Located in the Eastern Commonwealth of what was pre war America, it is now a major trade hub for travelers and merchants.\n",
                GeneralContents = "The Hub of Arroyo is a large, populated location and has many storage containers and guards.  It also has tons of merchants as well... \n",
                Accessable = true,
                ExperiencePoints = 10
            },

            new SpaceTimeLocation
            {
                CommonName = "The Citadel",
                SpaceTimeLocationID = 2,
                UniversalDate = 652165,
                UniversalLocation = "Columbia Commonwealth",
                Description = "The Citadel is the headquarters for the Knights.  The Citadel is located just south east of the Arroyo Hub.  It was known once as the Pentagon but has now become known as the Citadel to the inhabitants.  It is the main location that your fathers faction eastern chapter is located at.  You may want to stop by... \n",
                Accessable = false,
                ExperiencePoints = 20
            },

            new SpaceTimeLocation
            {
                CommonName = "Vault 05",
                SpaceTimeLocationID = 3,
                UniversalDate = 652165,
                UniversalLocation = "Columbia Commonwealth",
                Description = "Vault 05, the Diplomatic Vault as it was known in the old world.  It may house those who are the descendants of senators and governors that were in America's Capital during the bombing.  Could have some interesting finds.  Not real friendly to those who are not a direct descendant... \n",
                Accessable = false,
                ExperiencePoints = 60
            },

            new SpaceTimeLocation
            {
                CommonName = "Abraham Lincoln Air Force Base",
                SpaceTimeLocationID = 4,
                UniversalDate = 652165,
                UniversalLocation = "Columbia Commonwealth",
                Description = "Abraham Lincoln Air Force Base was Americas number one Air Force base for the East Coast before the war.  It served diplomats and whatever else deemed necessary to land there instead of the Airport located 40 miles beside it.  It also houses a remnant faction of what they claim to be the true successor to the American Government.  Not very friendly to outsiders.\n",
                Accessable = true,
                ExperiencePoints = 10
            },

            new SpaceTimeLocation
            {
                CommonName = "Canvas City",
                SpaceTimeLocationID = 5,
                UniversalDate = 652165,
                UniversalLocation = "Eastern Commonwealth",
                Description = "Canvas City was the anwser to those who wanted to stay in the Eastern Commonwealth but not be apart of Arroyo.  Canvas City is what seems to be the perfect example of what a pre war city looks like in the post war world.\n",
                GeneralContents = "Canvas City is smaller than Arroyo, but none the less, it still holds a functioning bar, hospital, school, grocery store, and Canvas City Police.  Who knows, mayber you can get a room at the Canvas City Inn. \n",
                Accessable = true,
                ExperiencePoints = 10
            },

            new SpaceTimeLocation
            {
                CommonName = "Fort Andrews Military Base",
                SpaceTimeLocationID = 6,
                UniversalDate = 652165,
                UniversalLocation = "Columbia Commonwealth",
                Description = "Abraham Lincoln Air Force Base was Americas number one Air Force base for the East Coast before the war.  It served diplomats and whatever else deemed necessary to land there instead of the Airport located 40 miles beside it.  It also houses a remnant faction of what they claim to be the true successor to the American Government.  Not very friendly to outsiders.\n",
                Accessable = true,
                ExperiencePoints = 10
            }
        };
    }
}
