﻿using NoahsGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    /// <summary>
    /// controller for the MVC pattern in the application
    /// </summary>
    public class Controller
    {
        #region FIELDS

        public ConsoleView _gameConsoleView;
        private Traveler _gameTraveler;
        private Universe _gameUniverse;
        private bool _playingGame;
        private SpaceTimeLocation _currentLocation;

        #endregion

        #region PROPERTIES


        #endregion

        #region CONSTRUCTORS

        public Controller()
        {
            //
            // setup all of the objects in the game
            //
            InitializeGame();

            //
            // begins running the application UI
            //
            ManageGameLoop();
        }

        #endregion

        #region METHODS

        /// <summary>
        /// initialize the major game objects
        /// </summary>
        private void InitializeGame()
        {
            _gameTraveler = new Traveler();
            _gameUniverse = new Universe();
            _gameConsoleView = new ConsoleView(_gameTraveler, _gameUniverse);
            Object travelerObject;
            _playingGame = true;

            foreach (GameObject gameObject in _gameUniverse.GameObjects)
            {
                if (gameObject is Object)
                {
                    travelerObject = gameObject as Object;
                    travelerObject.ObjectAddedToInventory += HandleObjectAddedToInventory;
                }
            }

            _gameTraveler.Inventory.Add(_gameUniverse.GetGameObjectById(5) as Object); // Bip-Man
            _gameTraveler.Inventory.Add(_gameUniverse.GetGameObjectById(6) as Object); // Lucky 14 Canteen

            Console.CursorVisible = false;
        }

        /// <summary>
        /// method to manage the application setup and game loop
        /// </summary>
        private void ManageGameLoop()
        {
            TravelerAction travelerActionChoice = TravelerAction.None;

            //
            // display splash screen
            //
            _playingGame = _gameConsoleView.DisplaySpashScreen();

            //
            // player chooses to quit
            //
            if (!_playingGame)
            {
                Environment.Exit(1);
            }


            

            //
            // display introductory message
            //
            _gameConsoleView.DisplayGamePlayScreen("Mission Intro", Text.MissionIntro(), ActionMenu.MissionIntro, "");
            _gameConsoleView.GetContinueKey();

            //
            // initialize the mission traveler
            // 
            InitializeMission();

            //
            // prepare game play screen
            //
            _gameConsoleView.DisplayGamePlayScreen("Current Location", Text.CurrrentLocationInfo(), ActionMenu.MainMenu, "");
            _currentLocation = _gameUniverse.GetSpaceTimeLocationById(_gameTraveler.SpaceTimeLocationID);

            //
            // game loop
            //
            while (_playingGame)
            {

                UpdateGameStatus();

                //
                // get next game action from player
                //
                if (ActionMenu.currentMenu == ActionMenu.CurrentMenu.MainMenu)
                {
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.MainMenu);
                }
                else if (ActionMenu.currentMenu == ActionMenu.CurrentMenu.AdminMenu)
                {
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.AdminMenu);
                }
                else if (ActionMenu.currentMenu == ActionMenu.CurrentMenu.TravelerMenu)
                {
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.TravelerMenu);
                }
                else if (ActionMenu.currentMenu == ActionMenu.CurrentMenu.ObjectMenu)
                {
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.ObjectMenu);
                }
                else if (ActionMenu.currentMenu == ActionMenu.CurrentMenu.NPCMenu)
                {
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.NPCMenu);
                }

                //
                // choose an action based on the player's menu choice
                //
                switch (travelerActionChoice)
                {
                    case TravelerAction.None:
                        break;

                    case TravelerAction.TravelerInfo:
                        _gameConsoleView.DisplayTravelerInfo();
                        break;

                    case TravelerAction.TravelerEdit:
                        _gameConsoleView.DisplayChangeTravelerInfo(_gameTraveler);
                        break;
                   
                    case TravelerAction.ListSpaceTimeLocations:
                        _gameConsoleView.DisplayListOfSpaceTimeLocations();
                        break;

                    case TravelerAction.LookAround:
                        _gameConsoleView.DisplayLookAround();
                        break;

                    case TravelerAction.LookAt:
                        LookAtAction();
                        break;

                    case TravelerAction.PickUp:
                        PickUpAction();
                        break;

                    case TravelerAction.PutDown:
                        PutDownAction();
                        break;

                    case TravelerAction.TalkTo:
                        TalkToAction();
                        break;

                    case TravelerAction.ListAllMerchantWares:
                        TalkToAction();
                        break;

                    case TravelerAction.Inventory:
                        _gameConsoleView.DisplayInventory();
                        break;

                    case TravelerAction.Travel:
                        CheckPlayerClassStatus();
                        UpdateAccessibility();
                        _gameTraveler.SpaceTimeLocationID = _gameConsoleView.DisplayNextSpaceTimeLocation();
                        _currentLocation = _gameUniverse.GetSpaceTimeLocationById(_gameTraveler.SpaceTimeLocationID);
                        _gameConsoleView.DisplayGamePlayScreen("Current Location", Text.CurrentLocationInfo(_currentLocation), ActionMenu.MainMenu, "");
                        break;

                    case TravelerAction.VisitedLocations:
                        _gameConsoleView.DisplayLocationsVisited();
                        break;

                    case TravelerAction.ListGameObjects:
                        _gameConsoleView.DisplayListOfAllGameObjects();
                        break;

                    case TravelerAction.NonplayerCharacters:
                        _gameConsoleView.DisplayListOfAllNPCObjects();
                        break;

                    case TravelerAction.AdminMenu:
                        ActionMenu.currentMenu = ActionMenu.CurrentMenu.AdminMenu;
                        _gameConsoleView.DisplayGamePlayScreen("Admin Menu", "Select an operation from the menu", ActionMenu.AdminMenu, "");
                        break;

                    case TravelerAction.TravelerMenu:
                        ActionMenu.currentMenu = ActionMenu.CurrentMenu.TravelerMenu;
                        _gameConsoleView.DisplayGamePlayScreen("Traveler Menu", "Select an operation from the menu", ActionMenu.TravelerMenu, "");
                        break;

                    case TravelerAction.ObjectMenu:
                        ActionMenu.currentMenu = ActionMenu.CurrentMenu.ObjectMenu;
                        _gameConsoleView.DisplayGamePlayScreen("Object Menu", "Select an operation from the menu", ActionMenu.ObjectMenu, "");
                        break;

                    case TravelerAction.NPCMenu:
                        _gameUniverse.TheStuff();
                        ActionMenu.currentMenu = ActionMenu.CurrentMenu.NPCMenu;
                        _gameConsoleView.DisplayGamePlayScreen("NPC Menu", "Select an operation from the menu", ActionMenu.NPCMenu, "");
                        break;

                    case TravelerAction.ReturnToMainMenu:
                        ActionMenu.currentMenu = ActionMenu.CurrentMenu.MainMenu;
                        _gameConsoleView.DisplayGamePlayScreen("Current Location", Text.CurrentLocationInfo(_currentLocation), ActionMenu.MainMenu, "");
                        break;

                    case TravelerAction.Exit:
                        _playingGame = _gameConsoleView.DisplayClosingScreen();
                        break;

                    default:
                        break;
                }
            }

            //
            // close the application
            //
            Environment.Exit(1);
        }

        /// <summary>
        /// initialize the player info
        /// </summary>
        private void InitializeMission()
        {
            Traveler traveler = _gameConsoleView.GetInitialTravelerInfo();

            _gameTraveler.Name = traveler.Name;
            _gameTraveler.Age = traveler.Age;
            _gameTraveler.HomeLocation = traveler.HomeLocation;
            _gameTraveler.Class = traveler.Class;
            _gameTraveler.SpaceTimeLocationID = 1;
            _gameTraveler.Special = traveler.Special;

            _gameTraveler.ExperiencePoints = 0;
            _gameTraveler.Health = 100;
            _gameTraveler.Lives = 6;

        }

        /// <summary>
        /// Updates Game Status
        /// </summary>
        private void UpdateGameStatus()
        {
            if (!_gameTraveler.HasVisited(_currentLocation.SpaceTimeLocationID))
            {
                _gameTraveler.SpaceTimeLocationsVisited.Add(_currentLocation.SpaceTimeLocationID);

                _gameTraveler.ExperiencePoints += _currentLocation.ExperiencePoints;
            }
        }

        /// <summary>
        /// Updates Location Access
        /// </summary>
        private void UpdateAccessibility() {
            _gameUniverse.UpdateLocation(_gameTraveler);
        }

        private void CheckPlayerClassStatus()
        {
            if (_gameTraveler.Class == Character.ClassType.None)
            {
                _gameConsoleView.DisplayGamePlayScreen("Character Setup - Race", Text.InitializeMissionGetTravelerClass(_gameTraveler), ActionMenu.MainMenu, "");
                _gameConsoleView.DisplayInputBoxPrompt($"Enter your race {_gameTraveler.Name} (If none is chosen, one will be appointed at Random): ");
                _gameTraveler.Class = _gameConsoleView.GetRace();

                if (_gameTraveler.Class == Traveler.ClassType.None)
                {
                    Random rnd = new Random();
                    int randNum = rnd.Next(1, 8);

                    switch (randNum)
                    {
                        case 1:
                            _gameTraveler.Class = Traveler.ClassType.Doctor;
                            break;

                        case 2:
                            _gameTraveler.Class = Traveler.ClassType.Dweller;
                            break;

                        case 3:
                            _gameTraveler.Class = Traveler.ClassType.Legionaire;
                            break;

                        case 4:
                            _gameTraveler.Class = Traveler.ClassType.Remnant;
                            break;

                        case 5:
                            _gameTraveler.Class = Traveler.ClassType.Soldier;
                            break;

                        case 6:
                            _gameTraveler.Class = Traveler.ClassType.Wanderer;
                            break;

                        case 7:
                            _gameTraveler.Class = Traveler.ClassType.Wastelander;
                            break;

                        default:
                            break;
                    }

                }

            }
        }

        /// <summary>
        /// Displays Look At
        /// </summary>
        private void LookAtAction(){

            int gameObjectToLookAtID = _gameConsoleView.DisplayGetGameObjectToLookAt();

            if (gameObjectToLookAtID != 0)
            {
                GameObject gameObject = _gameUniverse.GetGameObjectById(gameObjectToLookAtID);
                _gameConsoleView.DisplayGameObjectInfo(gameObject);
            }
        }

        /// <summary>
        /// Method that picks up a game object
        /// </summary>
        private void PickUpAction()
        {
            int gameObjectToPickUpID = _gameConsoleView.DisplayGetGameObjectToPickUp();

            if (gameObjectToPickUpID != 0)
            {
                Object gameObject = _gameUniverse.GetGameObjectById(gameObjectToPickUpID) as Object;
                _gameTraveler.Inventory.Add(gameObject);
                gameObject.LocationId = 0;
                _gameConsoleView.DisplayConfirmGameObjectAddedToInventory(gameObject);
            }

        }

        /// <summary>
        /// Method that put downs game objects
        /// </summary>
        private void PutDownAction()
        {
            int inventoryObjectToPutDownID = _gameConsoleView.DisplayGetInventoryObjectToPutDown();
            Object gameObject = _gameUniverse.GetGameObjectById(inventoryObjectToPutDownID) as Object;
            _gameTraveler.Inventory.Remove(gameObject);
            _gameConsoleView.DisplayConfirmTravelerObjectRemovedFromInventory(gameObject);

        }

        /// <summary>
        /// Talk to action method
        /// </summary>
        private void TalkToAction()
        {
            int npcToTalkToID = _gameConsoleView.DisplayGetNPCToTalkTo();

            if (npcToTalkToID != 0)
            {
                NPC npc = _gameUniverse.GetNPCByID(npcToTalkToID);

                _gameConsoleView.DisplayTalkTo(npc);

            }

        }

        private void MerchantWares()
        {
            int npcToTalkToID = _gameConsoleView.DisplayGetNPCToTalkTo();

            if (npcToTalkToID != 0)
            {
                NPC npc = _gameUniverse.GetNPCByID(npcToTalkToID);

                _gameConsoleView.DisplayTalkTo(npc);

            }
        }

        /// <summary>
        /// Gets travlers next action
        /// </summary>
        /// <returns></returns>
        private TravelerAction GetNextTravelerAction()
        {
            TravelerAction travelerActionChoice = TravelerAction.None;

            switch (ActionMenu.currentMenu)
            {
                case ActionMenu.CurrentMenu.MainMenu:
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.MainMenu);
                    break;
                case ActionMenu.CurrentMenu.ObjectMenu:
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.ObjectMenu);
                    break;
                case ActionMenu.CurrentMenu.NPCMenu:
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.NPCMenu);
                    break;
                case ActionMenu.CurrentMenu.TravelerMenu:
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.TravelerMenu);
                    break;
                case ActionMenu.CurrentMenu.AdminMenu:
                    travelerActionChoice = _gameConsoleView.GetActionMenuChoice(ActionMenu.AdminMenu);
                    break;
                default:
                    break;
            }

            return travelerActionChoice;
        }

        private void HandleObjectAddedToInventory(object gameObject, EventArgs e)
        {
            if (gameObject.GetType() == typeof(Object))
            {
                Object travelerObject = gameObject as Object;
                switch (travelerObject.Type)
                {
                    case ObjectType.Food:
                        _gameTraveler.Health += travelerObject.Value;

                        if (_gameTraveler.Health <= 99)
                        {
                            _gameTraveler.Health += travelerObject.Value;
                        }

                        if (travelerObject.IsConsumable)
                        {
                            travelerObject.LocationId = -1;
                        }
                        break;
                    case ObjectType.Medicine:
                        _gameTraveler.Health += travelerObject.Value;

                        if (_gameTraveler.Health >= 100)
                        {
                            _gameTraveler.Health = 100;
                            _gameTraveler.Lives += 1;
                        }

                        if (travelerObject.IsConsumable)
                        {
                            travelerObject.LocationId = -1;
                        }

                        break;
                    case ObjectType.Weapon:
                        break;
                    case ObjectType.Treasure:
                        break;
                    case ObjectType.Information:
                        break;
                    default:
                        break;
                }

            }
        }

        #endregion
    }
}
