﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    /// <summary>
    /// base class for the player and all game characters
    /// </summary>
    public class Character
    {
        #region ENUMERABLES

        public enum ClassType
        {
            None,
            Dweller,
            Wastelander,
            Remnant,
            Soldier,
            Legionaire,
            Doctor,
            Wanderer
        }
        #endregion

        #region FIELDS

        private string _name;
        private int _spaceTimeLocationID;
        private int _age;
        private bool _alive;
        private ClassType _class;

        #endregion

        #region PROPERTIES

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int SpaceTimeLocationID
        {
            get { return _spaceTimeLocationID; }
            set { _spaceTimeLocationID = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public bool Alive
        {
            get { return _alive; }
            set { _alive = value; }
        }

        public ClassType Class
        {
            get { return _class; }
            set { _class = value; }
        }

        #endregion

        #region CONSTRUCTORS

        public Character()
        {

        }

        public Character(string name, ClassType race, int spaceTimeLocationID, bool alive)
        {
            _name = name;
            _class = race;
            _spaceTimeLocationID = spaceTimeLocationID;
            _alive = alive;
        }

        #endregion

        #region METHODS

        public virtual string FirstContact()
        {
            return $"Hey Wastelander, name is {_name}.  Guess you could say I'm a {_class}";
        }

        #endregion
    }
}
