﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    public class Civilian : NPC, ISpeak, IBuy
    {
        public override int ID { get; set; }
        public override string Description { get; set; }
        public List<string> Messages { get; set; }
        public List<GameObject> merchantWares { get; set; }



        public string Buy()
        {
            Random r = new Random();
            int objectIndex = r.Next(0, 9);

            return "";
        }

        public string NothingToSell()
        {
            return "I currently do not have any goods to sell.  Sorry";
        }

        public string Speak()
        {
            if (this.Messages != null)
            {
                return GetMessage();
            } else
            {
                return $"My name is {base.Name} and I am a {base.Class}";
            }
        }

        private string GetMessage()
        {
            Random r = new Random();
            int messageIndex = r.Next(0, Messages.Count());
            return Messages[messageIndex];
        }

    }
}
