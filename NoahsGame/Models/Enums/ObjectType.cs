﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{

    public enum ObjectType {
        Food,
        Medicine,
        Weapon,
        Treasure,
        Information
    }

}
