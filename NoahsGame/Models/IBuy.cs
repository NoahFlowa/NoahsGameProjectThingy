﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    interface IBuy
    {
        List<GameObject> merchantWares { get; set; }
        string Buy();
    }
}
