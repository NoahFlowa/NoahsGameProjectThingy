﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    public class Object : GameObject
    {
        public override int Id { get; set; }
        public override string Name { get; set; }
        public override string Description { get; set; }
        private int _Location;

        public ObjectType Type { get; set; }
        public bool CanInventory { get; set; }
        public bool IsConsumable { get; set; }
        public bool IsVisible { get; set; }
        public int Value { get; set; }

        public string PickUpMessage { get; set; }
        public string PutDownMessage { get; set; }
        public event EventHandler ObjectAddedToInventory;

        public void OnObjectAddedToInventory()
        {
            if (ObjectAddedToInventory != null)
            {
                ObjectAddedToInventory(this, EventArgs.Empty);
            }
        }

        public override int LocationId
        {
            get
            {
                return _Location;
            }

            set
            {
                _Location = value;
                if (value == 0)
                {
                    OnObjectAddedToInventory();
                }
            }

        }

    }

}
