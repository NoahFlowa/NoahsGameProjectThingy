﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    /// <summary>
    /// the character class the player uses in the game
    /// </summary>
    public class Traveler : Character
    {
        #region ENUMERABLES
        public enum SPECIAL
        {
            None,
            Strong,
            Perceptive,
            Endurability,
            Charismatic,
            Intelligent,
            Agile,
            Lucky
        }

        #endregion

        #region FIELDS
        private SPECIAL _special;
        private string _homeLoc;
        private List<int> _spaceTimeLocationsVisited;
        private int _experiencePoints;
        private int _health;
        private int _lives;
        private List<Object> _inventory;

        #endregion


        #region PROPERTIES
        public SPECIAL Special
        {
            get { return _special; }
            set { _special = value; }
        }

        public string HomeLocation
        {
            get { return _homeLoc; }
            set { _homeLoc = value; }
        }

        public List<int> SpaceTimeLocationsVisited
        {
            get { return _spaceTimeLocationsVisited; }
            set { _spaceTimeLocationsVisited = value; }
        }

        public int ExperiencePoints
        {
            get { return _experiencePoints; }
            set { _experiencePoints = value; }
        }

        public int Health
        {
            get { return _health; }
            set { _health = value; }
        }

        public int Lives
        {
            get { return _lives; }
            set { _lives = value; }
        }

        public List<Object> Inventory
        {
            get { return _inventory; }
            set { _inventory = value; }
        }

        #endregion


        #region CONSTRUCTORS

        public Traveler()
        {
            _spaceTimeLocationsVisited = new List<int>();
            _inventory = new List<Object>();
        }

        public Traveler(string name, ClassType race, int spaceTimeLocationID, bool alive) : base(name, race, spaceTimeLocationID, alive)
        {
            _spaceTimeLocationsVisited = new List<int>();
            _inventory = new List<Object>();
        }

        #endregion


        #region METHODS

        public override string FirstContact()
        {
            return $"Howdy, the name is {base.Name}.  I'm from {HomeLocation}.";
        }

        public bool HasVisited(int _spaceTimeLocationID)
        {
            if (SpaceTimeLocationsVisited.Contains(_spaceTimeLocationID))
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        #endregion
    }
}
