﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    /// <summary>
    /// enum of all possible player actions
    /// </summary>
    public enum TravelerAction
    {
        None,
        MissionSetup,
        LookAround,
        Travel,

        ObjectMenu,
        LookAt,
        PickUp,
        PutDown,

        TravelerMenu,
        TravelerInfo,
        TravelerEdit,
        Inventory,
        VisitedLocations,

        AdminMenu,
        ListSpaceTimeLocations,
        ListGameObjects,
        ListItems,

        NonplayerCharacters,
        NPCMenu,
        TalkTo,
        ListAllMerchantWares,

        ReturnToMainMenu,
        Exit
    }
}
