﻿using NoahsGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    /// <summary>
    /// class of the game map
    /// </summary>
    public class Universe
    {
        #region ***** define all lists to be maintained by the Universe object *****

        //
        // List of all Space-Time Locations
        //

        private ConsoleView _gameConsoleView;
        private Controller _controller;
        private List<SpaceTimeLocation> _spaceTimeLocations;
        private List<GameObject> _gameObjects;
        private List<NPC> _npcs;

        public List<SpaceTimeLocation> SpaceTimeLocations {
            get { return _spaceTimeLocations; }
            set { _spaceTimeLocations = value; }
        }

        public List<GameObject> GameObjects
        {
            get { return _gameObjects; }
            set { _gameObjects = value; }
        }

        public List<NPC> NPCS
        {
            get { return _npcs; }
            set { _npcs = value; }
        }

        #endregion

        #region ***** constructor *****

        //
        // default Universe constructor
        //
        public Universe()
        {
            //
            // add all of the universe objects to the game
            // 
            IntializeUniverse();
        }

        #endregion

        #region ***** define methods to initialize all game elements *****

        /// <summary>
        /// initialize the universe with all of the space-time locations
        /// </summary>
        private void IntializeUniverse()
        {
            _spaceTimeLocations = UniverseObjectsLocations.SpaceTimeLocations;
            _gameObjects = UniversObjectsGameObjects.gameObjects;
            _npcs = UniverseNPCs.NPCs;

        }

        #endregion

        #region ***** define methods to return game element objects and information *****

        public List<NPC> GetNPCsBySpaceTimeLocationID(int spaceTimeLocationID)
        {
            List<NPC> NPCs = new List<NPC>();

            foreach (NPC npc in _npcs)
            {
                if (npc.SpaceTimeLocationID == spaceTimeLocationID)
                {
                    NPCs.Add(npc);
                }
            }

            return NPCs;
        }

        /// <summary>
        /// determine if the Space-Time Location Id is valid
        /// </summary>
        /// <param name="spaceTimeLocationId">true if Space-Time Location exists</param>
        /// <returns></returns>
        public bool IsValidSpaceTimeLocationId(int spaceTimeLocationId)
        {
            List<int> spaceTimeLocationIds = new List<int>();

            // Stuff for the Location Ids
            foreach (SpaceTimeLocation stl in _spaceTimeLocations)
            {
                spaceTimeLocationIds.Add(stl.SpaceTimeLocationID);
            }

            if (spaceTimeLocationIds.Contains(spaceTimeLocationId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidNPCByLocationID(int npcID, int currentSpaceTimeLocation)
        {
            List<int> npcIDs = new List<int>();

            foreach (NPC npc in _npcs)
            {
                if (npc.SpaceTimeLocationID == currentSpaceTimeLocation)
                {
                    npcIDs.Add(npc.ID);
                }
            }

            if (npcIDs.Contains(npcID))
            {
                return true;
            } else
            {
                return false;
            }


        }

        /// <summary>
        /// Ises the valid game object by location identifier.
        /// </summary>
        /// <returns><c>true</c>, if valid game object by location identifier was ised, <c>false</c> otherwise.</returns>
        /// <param name="gameObjectId">Game object identifier.</param>
        /// <param name="currentSpaceTimeLocation">Current space time location.</param>
        public bool IsValidGameObjectByLocationId (int gameObjectId, int currentSpaceTimeLocation)
        {
            List<int> gameObjectIds = new List<int>();

            foreach (GameObject gameObject  in _gameObjects)
            {
                if (gameObject.LocationId == currentSpaceTimeLocation)
                {
                    gameObjectIds.Add(gameObject.Id);
                }
            }

            if (gameObjectIds.Contains(gameObjectId))
            {
                return true;
            } else {
                return false;
            }

        }

        public List<GameObject> GetMerchantObjects()
        {
            List<GameObject> objectsToSendBack = new List<GameObject>();

            Random r = new Random();

            for (int i = 0; i < 5; i++)
            {
                int rNum = r.Next(1, GameObjects.Count + 1);

                GameObject generic;

                generic = GetGameObjectById(rNum);

                if (!objectsToSendBack.Contains(generic))
                {
                    objectsToSendBack.Add(generic);
                }
            }
            return objectsToSendBack;
        }

        public void TheStuff()
        {
            foreach (NPC thePeople in _npcs)
            {
                if (thePeople is Civilian)
                {
                    Civilian civilian = (Civilian)thePeople;

                    civilian.merchantWares = GetMerchantObjects();
                }
            }
        }

        /// <summary>
        /// Gets the game object by identifier.
        /// </summary>
        /// <returns>The game object by identifier.</returns>
        /// <param name="Id">Identifier.</param>
        public GameObject GetGameObjectById (int Id) {

            GameObject gameObjectToReturn = null;

            foreach (GameObject gameObject in _gameObjects )
            {
                if (gameObject.Id == Id)
                {
                    gameObjectToReturn = gameObject;
                }
            }

            if (gameObjectToReturn == null)
            {
                string feedbackMessage = $"The Game Object ID {Id} does not exist in the wasteland!";
                throw new ArgumentException(feedbackMessage, Id.ToString());
            }

            return gameObjectToReturn;
        }

        /// <summary>
        /// Gets the game object by space time location identifier.
        /// </summary>
        /// <returns>The game object by space time location identifier.</returns>
        /// <param name="spaceTimeLocationId">Space time location identifier.</param>
        public List<GameObject> GetGameObjectBySpaceTimeLocationId(int spaceTimeLocationId) {
            List<GameObject> gameObjects = new List<GameObject>();

            foreach (GameObject gameObject  in _gameObjects)
            {
                if (gameObject.LocationId == spaceTimeLocationId) {
                    gameObjects.Add(gameObject);
                }
            }

            return gameObjects;
        }

        /// <summary>
        /// Gets the space time location by identifier.
        /// </summary>
        /// <returns>The space time location by identifier.</returns>
        /// <param name="Id">Identifier.</param>
        public SpaceTimeLocation GetSpaceTimeLocationById(int Id)
        {
            SpaceTimeLocation spaceTimeLocation = null;

            foreach (SpaceTimeLocation location in _spaceTimeLocations)
            {
                if (location.SpaceTimeLocationID == Id)
                {
                    spaceTimeLocation = location;
                }
            }

            if (spaceTimeLocation == null)
            {
                string feedbackMessage = $"The Space-Time Location ID (Id) does not exist!";
                throw new ArgumentException(Id.ToString(), feedbackMessage);
            }

            return spaceTimeLocation;
        }

        public NPC GetNPCByID(int ID)
        {
            NPC npcToReturn = null;

            foreach (NPC npc in _npcs)
            {
                if (npc.ID == ID)
                {
                    npcToReturn = npc;
                }
            }

            if (npcToReturn == null)
            {
                string feedbackMessage = $"The NPC ID {ID} does not exist in the current Universe.";
                throw new ArgumentException(feedbackMessage, ID.ToString());
            }

            return npcToReturn;
        }

        /// <summary>
        /// determine if a location is accessible to the player
        /// </summary>
        /// <param name="spaceTimeLocationId"></param>
        /// <returns>accessible</returns>
        public bool IsAccessibleLocation(int spaceTimeLocationId)
        {
            SpaceTimeLocation spaceTimeLocation = GetSpaceTimeLocationById(spaceTimeLocationId);
            if (spaceTimeLocation.Accessable == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Object> TravelerInventory()
        {
            List<Object> inventory = new List<Object>();

            foreach (GameObject gameObject in _gameObjects)
            {
                if (gameObject.LocationId == 0)
                {
                    inventory.Add(gameObject as Object);
                }
            }

            return inventory;
        }

        /// <summary>
        /// return the next available ID for a SpaceTimeLocation object
        /// </summary>
        /// <returns>next SpaceTimeLocationObjectID </returns>
        public int GetMaxSpaceTimeLocationId()
        {
            int MaxId = 0;

            foreach (SpaceTimeLocation spaceTimeLocation in SpaceTimeLocations)
            {
                if (spaceTimeLocation.SpaceTimeLocationID > MaxId)
                {
                    MaxId = spaceTimeLocation.SpaceTimeLocationID;
                }
            }

            return MaxId;

        }

        /// <summary>
        /// Checks to see if location is accesible from current location
        /// </summary>
        /// <param name="traveler">Traveler.</param>
        public void UpdateLocation(Traveler traveler) {

           


            // At Arroyo
            if (traveler.SpaceTimeLocationID == 1)
            {
                foreach (SpaceTimeLocation l in SpaceTimeLocations)
                {
                    if (l.SpaceTimeLocationID == 5)
                    {
                        l.Accessable = true;
                    }
                    else
                    {
                        l.Accessable = false;
                    }
                }
            }
            // At Citadel
            else if (traveler.SpaceTimeLocationID == 2)
            {
                foreach (SpaceTimeLocation l in SpaceTimeLocations)
                {
                    if (l.SpaceTimeLocationID == 3)
                    {
                        l.Accessable = true;
                    }
                    else
                    {
                        l.Accessable = false;
                    }
                }
            }
            // At Vault 05
            else if (traveler.SpaceTimeLocationID == 3)
            {
                foreach (SpaceTimeLocation l in SpaceTimeLocations)
                {
                    if (l.SpaceTimeLocationID == 2)
                    {
                        l.Accessable = true;
                    }
                    else
                    {
                        l.Accessable = false;
                    }
                }
            }
            // At ALAFB
            else if (traveler.SpaceTimeLocationID == 4)
            {
                foreach (SpaceTimeLocation l in SpaceTimeLocations)
                {
                    if (l.SpaceTimeLocationID == 2 || l.SpaceTimeLocationID == 5 || l.SpaceTimeLocationID == 6 || l.SpaceTimeLocationID == 7 || l.SpaceTimeLocationID == 8)
                    {
                        l.Accessable = true;
                    }
                    else
                    {
                        l.Accessable = false;
                    }
                }
            }
            // At Canvas City
            else if (traveler.SpaceTimeLocationID == 5 || traveler.SpaceTimeLocationID == 6 || traveler.SpaceTimeLocationID == 7 || traveler.SpaceTimeLocationID == 8)
            {
                foreach (SpaceTimeLocation l in SpaceTimeLocations)
                {
                    if (l.SpaceTimeLocationID == 4)
                    {
                        l.Accessable = true;
                    }
                    else
                    {
                        l.Accessable = false;
                    }
                }
            }
        }

        #endregion
    }
}
