﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame.Models
{
    public static partial class UniverseNPCs
    {

        public static List<NPC> NPCs = new List<NPC>()
        {
            new Civilian
            {
                ID = 1,
                Name = "Gregg the Gardener",
                SpaceTimeLocationID = 1,
                Description = "A fat man with blue suspenders on.  Gregg is known for being the main farmer/gardener for Arroyo for years now.  You can get food from him, after you pay his Gardner fee...",
                Messages = new List<string>
                {
                    "Howdy there fellow, come to see what ole Gregg has in store fer ya?",
                    "I got all the corn you'll ever need",
                    "Stupid monkey has been driving me crazy all darn day"
                }
            },

            new Civilian
            {
                ID = 2,
                Name = "Isaac",
                SpaceTimeLocationID = 1,
                Description = "A tuff looking weapons dealer.  He doesnt look like he wants to be bothered with chit chat....",
                Messages = new List<string>
                {
                    "Grab your shit and get out",
                    "I got a real nice Repeater here for those pesky raiders",
                    "I dont do deals, unless you're friends with that monkey"
                }
            },

            new Civilian
            {
                ID = 3,
                Name = "The Monkey",
                SpaceTimeLocationID = 1,
                Description = "This monkey shouldn't be here, its way to hot for it to survive, yet it looks perfectly healthy...",
                Messages = new List<string>
                {
                    "Oo ooo ah ah",
                    "Ah ah oo ooO ah",
                    "Oo Ah Ah OooO"
                }
            },

            new Civilian
            {
                ID = 4,
                Name = "Arroyo Guard",
                SpaceTimeLocationID = 1,
                Description = "An Arroyo Guard, they dont have time for wastelanders....",
                Messages = new List<string>
                {
                    "Shouldn't you be banging rocks together?",
                    "Ih great, heres some of the local wildlife",
                    "Back to stare in awe of the Arroyo Guards are we?"
                }
            },


        };

    }
}
