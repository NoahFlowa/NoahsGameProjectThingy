﻿using NoahsGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoahsGame
{
    /// <summary>
    /// view class
    /// </summary>
    public class ConsoleView
    {
        #region ENUMS

        private enum ViewStatus
        {
            TravelerInitialization,
            PlayingGame
        }

        #endregion

        #region FIELDS

        //
        // declare game objects for the ConsoleView object to use
        //
        Traveler _gameTraveler;
        Universe _gameUniverse;

        ViewStatus _viewStatus;

        #endregion

        #region PROPERTIES

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// default constructor to create the console view objects
        /// </summary>
        public ConsoleView(Traveler gameTraveler, Universe gameUniverse)
        {
            _gameTraveler = gameTraveler;
            _gameUniverse = gameUniverse;

            _viewStatus = ViewStatus.TravelerInitialization;

            InitializeDisplay();
        }

        #endregion

        #region METHODS
        /// <summary>
        /// display all of the elements on the game play screen on the console
        /// </summary>
        /// <param name="messageBoxHeaderText">message box header title</param>
        /// <param name="messageBoxText">message box text</param>
        /// <param name="menu">menu to use</param>
        /// <param name="inputBoxPrompt">input box text</param>
        public void DisplayGamePlayScreen(string messageBoxHeaderText, string messageBoxText, Menu menu, string inputBoxPrompt)
        {
            //
            // reset screen to default window colors
            //
            Console.BackgroundColor = ConsoleTheme.WindowBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.WindowForegroundColor;
            Console.Clear();

            ConsoleWindowHelper.DisplayHeader(Text.HeaderText);
            ConsoleWindowHelper.DisplayFooter(Text.FooterText);

            DisplayMessageBox(messageBoxHeaderText, messageBoxText);
            DisplayMenuBox(menu);
            DisplayInputBox();
            DisplayStatusBox();
        }

        /// <summary>
        /// wait for any keystroke to continue
        /// </summary>
        public void GetContinueKey()
        {
            Console.ReadKey();
        }

        /// <summary>
        /// get a action menu choice from the user
        /// </summary>
        /// <returns>action menu choice</returns>
        public TravelerAction GetActionMenuChoice(Menu menu)
        {
            TravelerAction choosenAction = TravelerAction.None;
            Console.CursorVisible = false;

            char[] validKeys = menu.MenuChoices.Keys.ToArray();

            char keyPressed;
            do
            {
                ConsoleKeyInfo keyPressedInfo = Console.ReadKey();
                keyPressed = keyPressedInfo.KeyChar;
            } while (!validKeys.Contains(keyPressed));
            
            choosenAction = menu.MenuChoices[keyPressed];
            Console.CursorVisible = true;

            return choosenAction;
        }

        /// <summary>
        /// get a string value from the user
        /// </summary>
        /// <returns>string value</returns>
        public string GetString()
        {
            return Console.ReadLine();
        }

        public int DisplayGetGameObjectToLookAt() {
            int gameObjectId = 0;
            bool validGameObjectId = false;

            List<GameObject> gameObjectsInSpaceTimeLocation = _gameUniverse.GetGameObjectBySpaceTimeLocationId(_gameTraveler.SpaceTimeLocationID);

            if (gameObjectsInSpaceTimeLocation.Count > 0)
            {
                DisplayGamePlayScreen("Look at a object", Text.GameObjectsChooseList(gameObjectsInSpaceTimeLocation), ActionMenu.ObjectMenu, "");

                while (!validGameObjectId)
                {
                    GetInteger($"Enter the Id number of the objects you wish to look at: ", 0,0, out gameObjectId);

                    if (_gameUniverse.IsValidGameObjectByLocationId(gameObjectId, _gameTraveler.SpaceTimeLocationID))
                    {
                        validGameObjectId = true;
                    }
                    else {
                        ClearInputBox();
                        DisplayInputErrorMessage($"It appears you entered an invalid game object Id.  Please try again.");
                    }

                }

            }
            else {
                DisplayGamePlayScreen("Look at a object", "It appears there are no objects here.", ActionMenu.ObjectMenu, "");
            }
            return gameObjectId;
        }

        /// <summary>
        /// Displays the game object info.
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        public void DisplayGameObjectInfo(GameObject gameObject) {
            DisplayGamePlayScreen("Current Location", Text.LookAt(gameObject), ActionMenu.ObjectMenu, "");
        }

        /// <summary>
        /// Gets the integer.
        /// </summary>
        /// <returns><c>true</c>, if integer was gotten, <c>false</c> otherwise.</returns>
        /// <param name="prompt">Prompt.</param>
        /// <param name="minimumValue">Minimum value.</param>
        /// <param name="maximumValue">Maximum value.</param>
        /// <param name="integerChoice">Integer choice.</param>
        private bool GetInteger(string prompt, int minimumValue, int maximumValue, out int integerChoice)
        {
            bool validResponse = false;
            integerChoice = 0;

            bool validateRange = (minimumValue != 0 || maximumValue != 0);

            DisplayInputBoxPrompt(prompt);

            while (!validResponse)
            {
                if (int.TryParse(Console.ReadLine(), out integerChoice))
                {
                    if (validateRange)
                    {
                        if (integerChoice >= minimumValue && integerChoice <= maximumValue)
                        {
                            validResponse = true;
                        }
                        else
                        {
                            ClearInputBox();
                            DisplayInputErrorMessage($"You must enter an integer value between {minimumValue} and {maximumValue}.  Please try again.");
                            DisplayInputBoxPrompt(prompt);
                        }
                    }
                    else
                    {
                        validResponse = true;
                    }
                }
                else
                {
                    ClearInputBox();
                    DisplayInputErrorMessage($"You must enter an integer vallue.  Please try again.");
                    DisplayInputBoxPrompt(prompt);
                }
            }
            Console.CursorVisible = false;

            return true;
        }



        /// <summary>
        /// get a character race value from the user
        /// </summary>
        /// <returns>character race value</returns>
        public Character.ClassType GetRace()
        {
            Character.ClassType raceType;
            Enum.TryParse<Character.ClassType>(Console.ReadLine(), out raceType);

            return raceType;
        }

        /// <summary>
        /// get SPECIAL stats from player, can choose 3
        /// </summary>
        /// <returns></returns>
        public Traveler.SPECIAL GetSPECIAL()
        {
            Traveler.SPECIAL theirSPECIAL;
            Enum.TryParse<Traveler.SPECIAL>(Console.ReadLine(), out theirSPECIAL);

            return theirSPECIAL;
        }

        /// <summary>
        /// display splash screen
        /// </summary>
        /// <returns>player chooses to play</returns>
        public bool DisplaySpashScreen()
        {
            bool playing = true;
            ConsoleKeyInfo keyPressed;

            Console.BackgroundColor = ConsoleTheme.SplashScreenBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.SplashScreenForegroundColor;
            Console.Clear();
            Console.CursorVisible = false;


            Console.SetCursorPosition(0, 10);
            string tabSpace = new String(' ', 35);
            Console.WriteLine(tabSpace + @"  _______ _           __          __       _       _                 _           ");
            Console.WriteLine(tabSpace + @" |__   __| |          \ \        / /      | |     | |               | |          ");
            Console.WriteLine(tabSpace + @"    | |  | |__   ___   \ \  /\  / /_ _ ___| |_ ___| | __ _ _ __   __| | ___ _ __ ");
            Console.WriteLine(tabSpace + @"    | |  | '_ \ / _ \   \ \/  \/ / _` / __| __/ _ \ |/ _` | '_ \ / _` |/ _ \ '__|");
            Console.WriteLine(tabSpace + @"    | |  | | | |  __/    \  /\  / (_| \__ \ ||  __/ | (_| | | | | (_| |  __/ |   ");
            Console.WriteLine(tabSpace + @"    |_|  |_| |_|\___|     \/  \/ \__,_|___/\__\___|_|\__,_|_| |_|\__,_|\___|_|   ");
            Console.WriteLine(tabSpace + @"                                                                                 ");

            Console.SetCursorPosition(80, 25);
            Console.Write("Press any key to continue or Esc to exit.");
            keyPressed = Console.ReadKey();
            if (keyPressed.Key == ConsoleKey.Escape)
            {
                playing = false;
            }

            return playing;
        }

        /// <summary>
        /// initialize the console window settings
        /// </summary>
        private static void InitializeDisplay()
        {
            //
            // control the console window properties
            //
            ConsoleWindowControl.DisableResize();
            ConsoleWindowControl.DisableMaximize();
            ConsoleWindowControl.DisableMinimize();
            Console.Title = "The Wastelander";

            //
            // set the default console window values
            //
            ConsoleWindowHelper.InitializeConsoleWindow();

            Console.CursorVisible = false;
        }

        /// <summary>
        /// display the correct menu in the menu box of the game screen
        /// </summary>
        /// <param name="menu">menu for current game state</param>
        private void DisplayMenuBox(Menu menu)
        {
            Console.BackgroundColor = ConsoleTheme.MenuBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.MenuBorderColor;

            //
            // display menu box border
            //
            ConsoleWindowHelper.DisplayBoxOutline(
                ConsoleLayout.MenuBoxPositionTop,
                ConsoleLayout.MenuBoxPositionLeft,
                ConsoleLayout.MenuBoxWidth,
                ConsoleLayout.MenuBoxHeight);

            //
            // display menu box header
            //
            Console.BackgroundColor = ConsoleTheme.MenuBorderColor;
            Console.ForegroundColor = ConsoleTheme.MenuForegroundColor;
            Console.SetCursorPosition(ConsoleLayout.MenuBoxPositionLeft + 2, ConsoleLayout.MenuBoxPositionTop + 1);
            Console.Write(ConsoleWindowHelper.Center(menu.MenuTitle, ConsoleLayout.MenuBoxWidth - 4));

            //
            // display menu choices
            //
            Console.BackgroundColor = ConsoleTheme.MenuBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.MenuForegroundColor;
            int topRow = ConsoleLayout.MenuBoxPositionTop + 3;

            foreach (KeyValuePair<char, TravelerAction> menuChoice in menu.MenuChoices)
            {
                if (menuChoice.Value == TravelerAction.AdminMenu)
                {

                }
                else if (menuChoice.Value != TravelerAction.None)
                {
                    string formatedMenuChoice = ConsoleWindowHelper.ToLabelFormat(menuChoice.Value.ToString());
                    Console.SetCursorPosition(ConsoleLayout.MenuBoxPositionLeft + 3, topRow++);
                    Console.Write($"{menuChoice.Key}. {formatedMenuChoice}");
                }
            }
        }

        /// <summary>
        /// display the text in the message box of the game screen
        /// </summary>
        /// <param name="headerText"></param>
        /// <param name="messageText"></param>
        private void DisplayMessageBox(string headerText, string messageText)
        {
            //
            // display the outline for the message box
            //
            Console.BackgroundColor = ConsoleTheme.MessageBoxBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.MessageBoxBorderColor;
            ConsoleWindowHelper.DisplayBoxOutline(
                ConsoleLayout.MessageBoxPositionTop,
                ConsoleLayout.MessageBoxPositionLeft,
                ConsoleLayout.MessageBoxWidth,
                ConsoleLayout.MessageBoxHeight);

            //
            // display message box header
            //
            Console.BackgroundColor = ConsoleTheme.MessageBoxBorderColor;
            Console.ForegroundColor = ConsoleTheme.MessageBoxForegroundColor;
            Console.SetCursorPosition(ConsoleLayout.MessageBoxPositionLeft + 2, ConsoleLayout.MessageBoxPositionTop + 1);
            Console.Write(ConsoleWindowHelper.Center(headerText, ConsoleLayout.MessageBoxWidth - 4));

            //
            // display the text for the message box
            //
            Console.BackgroundColor = ConsoleTheme.MessageBoxBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.MessageBoxForegroundColor;
            List<string> messageTextLines = new List<string>();
            messageTextLines = ConsoleWindowHelper.MessageBoxWordWrap(messageText, ConsoleLayout.MessageBoxWidth - 4);

            int startingRow = ConsoleLayout.MessageBoxPositionTop + 3;
            int endingRow = startingRow + messageTextLines.Count();
            int row = startingRow;
            foreach (string messageTextLine in messageTextLines)
            {
                Console.SetCursorPosition(ConsoleLayout.MessageBoxPositionLeft + 2, row);
                Console.Write(messageTextLine);
                row++;
            }

        }

        /// <summary>
        /// draw the status box on the game screen
        /// </summary>
        public void DisplayStatusBox()
        {
            Console.BackgroundColor = ConsoleTheme.InputBoxBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.InputBoxBorderColor;

            //
            // display the outline for the status box
            //
            ConsoleWindowHelper.DisplayBoxOutline(
                ConsoleLayout.StatusBoxPositionTop,
                ConsoleLayout.StatusBoxPositionLeft,
                ConsoleLayout.StatusBoxWidth,
                ConsoleLayout.StatusBoxHeight);

            //
            // display the text for the status box if playing game
            //
            if (_viewStatus == ViewStatus.PlayingGame)
            {
                //
                // display status box header with title
                //
                Console.BackgroundColor = ConsoleTheme.StatusBoxBorderColor;
                Console.ForegroundColor = ConsoleTheme.StatusBoxForegroundColor;
                Console.SetCursorPosition(ConsoleLayout.StatusBoxPositionLeft + 2, ConsoleLayout.StatusBoxPositionTop + 1);
                Console.Write(ConsoleWindowHelper.Center("Game Stats", ConsoleLayout.StatusBoxWidth - 4));
                Console.BackgroundColor = ConsoleTheme.StatusBoxBackgroundColor;
                Console.ForegroundColor = ConsoleTheme.StatusBoxForegroundColor;

                //
                // display stats
                //
                int startingRow = ConsoleLayout.StatusBoxPositionTop + 3;
                int row = startingRow;
                foreach (string statusTextLine in Text.StatusBox(_gameTraveler))
                {
                    Console.SetCursorPosition(ConsoleLayout.StatusBoxPositionLeft + 3, row);
                    Console.Write(statusTextLine);
                    row++;
                }
            }
            else
            {
                //
                // display status box header without header
                //
                Console.BackgroundColor = ConsoleTheme.StatusBoxBorderColor;
                Console.ForegroundColor = ConsoleTheme.StatusBoxForegroundColor;
                Console.SetCursorPosition(ConsoleLayout.StatusBoxPositionLeft + 2, ConsoleLayout.StatusBoxPositionTop + 1);
                Console.Write(ConsoleWindowHelper.Center("", ConsoleLayout.StatusBoxWidth - 4));
                Console.BackgroundColor = ConsoleTheme.StatusBoxBackgroundColor;
                Console.ForegroundColor = ConsoleTheme.StatusBoxForegroundColor;
            }
        }

        /// <summary>
        /// draw the input box on the game screen
        /// </summary>
        public void DisplayInputBox()
        {
            Console.BackgroundColor = ConsoleTheme.InputBoxBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.InputBoxBorderColor;

            ConsoleWindowHelper.DisplayBoxOutline(
                ConsoleLayout.InputBoxPositionTop,
                ConsoleLayout.InputBoxPositionLeft,
                ConsoleLayout.InputBoxWidth,
                ConsoleLayout.InputBoxHeight);
        }

        /// <summary>
        /// display the prompt in the input box of the game screen
        /// </summary>
        /// <param name="prompt"></param>
        public void DisplayInputBoxPrompt(string prompt)
        {
            Console.SetCursorPosition(ConsoleLayout.InputBoxPositionLeft + 4, ConsoleLayout.InputBoxPositionTop + 1);
            Console.ForegroundColor = ConsoleTheme.InputBoxForegroundColor;
            Console.Write(prompt);
            Console.CursorVisible = true;
        }

        /// <summary>
        /// display the error message in the input box of the game screen
        /// </summary>
        /// <param name="errorMessage">error message text</param>
        public void DisplayInputErrorMessage(string errorMessage)
        {
            Console.SetCursorPosition(ConsoleLayout.InputBoxPositionLeft + 4, ConsoleLayout.InputBoxPositionTop + 2);
            Console.ForegroundColor = ConsoleTheme.InputBoxErrorMessageForegroundColor;
            Console.Write(errorMessage);
            Console.ForegroundColor = ConsoleTheme.InputBoxForegroundColor;
            Console.CursorVisible = true;
        }

        /// <summary>
        /// clear the input box
        /// </summary>
        private void ClearInputBox()
        {
            string backgroundColorString = new String(' ', ConsoleLayout.InputBoxWidth - 4);

            Console.ForegroundColor = ConsoleTheme.InputBoxBackgroundColor;
            for (int row = 1; row < ConsoleLayout.InputBoxHeight - 2; row++)
            {
                Console.SetCursorPosition(ConsoleLayout.InputBoxPositionLeft + 4, ConsoleLayout.InputBoxPositionTop + row);
                DisplayInputBoxPrompt(backgroundColorString);
            }
            Console.ForegroundColor = ConsoleTheme.InputBoxForegroundColor;
        }

        public int DisplayGetNPCToTalkTo()
        {
            int npcID = 0;
            bool validNPCID = false;

            List<NPC> npcsInSpaceTimeLocation = _gameUniverse.GetNPCsBySpaceTimeLocationID(_gameTraveler.SpaceTimeLocationID);

            if (npcsInSpaceTimeLocation.Count > 0)
            {
                DisplayGamePlayScreen("Choose character to Speak with", Text.NPCChooseList(npcsInSpaceTimeLocation), ActionMenu.NPCMenu, "");

                while (!validNPCID)
                {
                    GetInteger($"Enter the ID number of the character you wish to speak with: ", 0, 0, out npcID);

                    if (_gameUniverse.IsValidNPCByLocationID(npcID, _gameTraveler.SpaceTimeLocationID))
                    {
                        NPC npc = _gameUniverse.GetNPCByID(npcID);

                        if (npc is ISpeak)
                        {
                            validNPCID = true;
                        } else
                        {
                            ClearInputBox();
                            DisplayInputErrorMessage("It appears this character has nothing to say.  Please try again");
                        }

                    } else
                    {
                        ClearInputBox();
                        DisplayInputErrorMessage("It appears you used an invalid ID.  Please try again");
                    }

                }

            } else
            {
                DisplayGamePlayScreen("Choose character to speak with", "It appears there are no NPCs here.", ActionMenu.NPCMenu, "");
            }

            return npcID;
        }

        /// <summary>
        /// get the player's initial information at the beginning of the game
        /// </summary>
        /// <returns>traveler object with all properties updated</returns>
        public Traveler GetInitialTravelerInfo()
        {
            Traveler traveler = new Traveler();


            //
            // intro
            //
            DisplayGamePlayScreen("Character Setup", Text.InitializeMissionIntro(), ActionMenu.MissionIntro, "");
            GetContinueKey();

            //
            // get traveler's name
            //
            DisplayGamePlayScreen("Character Setup - Name", Text.InitializeMissionGetTravelerName(), ActionMenu.MissionIntro, "");
            DisplayInputBoxPrompt("Enter your name: ");
            traveler.Name = GetString();

            //
            // get traveler's age
            //
            DisplayGamePlayScreen("Character Setup - Age", Text.InitializeMissionGetTravelerAge(traveler.Name), ActionMenu.MissionIntro, "");
            int gameTravelerAge;

            GetInteger($"Enter your age {traveler.Name}: ", 0, 1000, out gameTravelerAge);
            traveler.Age = gameTravelerAge;

            //
            // get traveler's home location
            //
            DisplayGamePlayScreen("Character Setup - Location", Text.InitializeMissionGetTravelerHomeLocation(traveler.Name), ActionMenu.MissionIntro, "");
            DisplayInputBoxPrompt("Enter your home location (Press enter to make it Arroyo): ");
            traveler.HomeLocation = GetString();

            if (traveler.HomeLocation == "" || traveler.HomeLocation == null)
            {
                traveler.HomeLocation = "Arroyo";
            }

            //
            // get traveler's race
            //
            DisplayGamePlayScreen("Character Setup - Race", Text.InitializeMissionGetTravelerClass(traveler), ActionMenu.MissionIntro, "");
            DisplayInputBoxPrompt($"Enter your race {traveler.Name} (Press enter to change this later): ");
            traveler.Class = GetRace();

            //
            // get traveler's SPECIAL
            //
            DisplayGamePlayScreen("Character Setup - SPECIAL", Text.InitializeMissionGetTravelerSpecial(traveler), ActionMenu.MissionIntro, "");
            DisplayInputBoxPrompt($"Choose a starting SPECIAL stat {traveler.Name} (Press enter to change this later): ");
            traveler.Special = GetSPECIAL();

            //
            // echo the traveler's info
            //
            DisplayGamePlayScreen("Character Setup - Complete", Text.InitializeMissionEchoTravelerInfo(traveler), ActionMenu.MissionIntro, "");
            GetContinueKey();

            // 
            // change view status to playing game
            //
            _viewStatus = ViewStatus.PlayingGame;

            return traveler;
        }

        /// <summary>
        /// change the players name, class type if none was selected and SPECIAL if none was selected
        /// </summary>
        /// <param name="wastelander"></param>
        /// <returns></returns>
        public Traveler DisplayChangeTravelerInfo(Traveler wastelander)
        {
            Traveler traveler = wastelander;

            // 
            // change name
            //
            DisplayGamePlayScreen("Character Edit - Name", Text.InitializeMissionGetTravelerName(), ActionMenu.MissionIntro, "");
            DisplayInputBoxPrompt("Enter your name: ");
            traveler.Name = GetString();


            // Allows editing of player class type if none was selected at start
            if (traveler.Class == Traveler.ClassType.None)
            {
                //
                // get traveler's race
                //
                DisplayGamePlayScreen("Character Setup - Race", Text.InitializeMissionGetTravelerClass(traveler), ActionMenu.MissionIntro, "");
                DisplayInputBoxPrompt($"Enter your race {traveler.Name}: ");
                traveler.Class = GetRace();
            }

            // Allows editing of player traveler type if none was selected at start
            if (traveler.Special == Traveler.SPECIAL.None)
            {
                //
                // get traveler's SPECIAL
                //
                DisplayGamePlayScreen("Character Setup - SPECIAL", Text.InitializeMissionGetTravelerSpecial(traveler), ActionMenu.MissionIntro, "");
                DisplayInputBoxPrompt($"Choose a starting SPECIAL stat {traveler.Name}: ");
                traveler.Special = GetSPECIAL();
            }

            //
            // echo the traveler's info
            //
            DisplayGamePlayScreen("Character Edit - Complete", Text.DisplayChangedTravelerInfo(traveler), ActionMenu.MainMenu, "");

            // 
            // change view status to playing game
            //
            _viewStatus = ViewStatus.PlayingGame;

            return wastelander;
        }

        #region ----- display responses to menu action choices -----

        /// <summary>
        /// Displays the traveler info.
        /// </summary>
        public void DisplayTravelerInfo()
        {
            DisplayGamePlayScreen("Wastelander Information", Text.TravelerInfo(_gameTraveler), ActionMenu.TravelerMenu, "");
        }

        public void DisplayTalkTo(NPC npc)
        {
            ISpeak speakingNPC = npc as ISpeak;

            string message = speakingNPC.Speak();

            if (message == "")
            {
                message = "It appears this character has nothing to say.  Please try again";
            }
            DisplayGamePlayScreen("Speak to Character", message, ActionMenu.NPCMenu, "");
        }

        public void DisplayMerchantWares(NPC npc)
        {
            IBuy buyFromNPC = npc as IBuy;

            string message = buyFromNPC.Buy();

            if (message == "")
            {
                message = "It appears this merchant is too poor to buy stuff or doesnt have anything to sell";
            }

            DisplayGamePlayScreen("Buy From Merchant", message, ActionMenu.NPCMenu, "");

        }

        /// <summary>
        /// Displays the list of space time locations.
        /// </summary>
        public void DisplayListOfSpaceTimeLocations() {
            DisplayGamePlayScreen("List: Locations", Text.ListSpaceTimeLocations(_gameUniverse.SpaceTimeLocations), ActionMenu.AdminMenu, "");
        }

        /// <summary>
        /// Displays object to put down
        /// </summary>
        /// <returns></returns>
        public int DisplayGetInventoryObjectToPutDown()
        {
            int gameObjectID = 0;
            bool validInventoryObjectID = false;

            if (_gameTraveler.Inventory.Count > 0)
            {
                DisplayGamePlayScreen("Put down Game Object", Text.GameObjectsChooseList(_gameTraveler.Inventory), ActionMenu.ObjectMenu, "");

                while (!validInventoryObjectID)
                {
                    GetInteger($"Enter the ID number of the object you wish to remove from your inventory: ", 0, 0, out gameObjectID);
                    GameObject gameObjectToPutDown = _gameTraveler.Inventory.FirstOrDefault(o => o.Id == gameObjectID);

                    if (gameObjectToPutDown != null)
                    {
                        validInventoryObjectID = true;
                    } else
                    {
                        ClearInputBox();
                        DisplayInputErrorMessage("It appears you entered the ID of an object that is not in your inventory.  Please try again");
                    }
                    
                }

            } else
            {
                DisplayGamePlayScreen("Pick up game object", "It appears there are no objects currently in inventory", ActionMenu.ObjectMenu, "");
            }

            return gameObjectID;

        }

        public void DisplayConfirmTravelerObjectRemovedFromInventory(GameObject objectRemovedFromInventory)
        {
            DisplayGamePlayScreen("Put down game object", $"The {objectRemovedFromInventory.Name} has been removed from your inventory", ActionMenu.MainMenu, "");
        }

        /// <summary>
        /// Displaies the look around.
        /// </summary>
        public void DisplayLookAround()
        {
            SpaceTimeLocation currentSpaceTimeLocation = _gameUniverse.GetSpaceTimeLocationById(_gameTraveler.SpaceTimeLocationID);

            List<GameObject> gameObjectsInCurrentSpaceTimeLocation = _gameUniverse.GetGameObjectBySpaceTimeLocationId(_gameTraveler.SpaceTimeLocationID);

            List<NPC> npcsInCurrentSpaceTimeLocation = _gameUniverse.GetNPCsBySpaceTimeLocationID(_gameTraveler.SpaceTimeLocationID);

            string messageBoxText = Text.LookAround(currentSpaceTimeLocation) + Environment.NewLine;
            messageBoxText += Text.GameObjectsChooseList(gameObjectsInCurrentSpaceTimeLocation) + Environment.NewLine;
            messageBoxText += Text.NPCsChooseList(npcsInCurrentSpaceTimeLocation);

            DisplayGamePlayScreen("Current Location", messageBoxText, ActionMenu.MainMenu, "");
        }

        public void DisplayInventory()
        {
            DisplayGamePlayScreen("Current Inventory", Text.CurrentInventory(_gameTraveler.Inventory), ActionMenu.TravelerMenu, "");
        }

        public int DisplayGetGameObjectToPickUp()
        {
            int gameObjectID = 0;
            bool validGameObjectID = false;

            List<GameObject> gameObjectsInLocation = _gameUniverse.GetGameObjectBySpaceTimeLocationId(_gameTraveler.SpaceTimeLocationID);

            if (gameObjectsInLocation.Count > 0)
            {
                DisplayGamePlayScreen("Pick up game object", Text.GameObjectsChooseList(gameObjectsInLocation), ActionMenu.ObjectMenu, "");

                while (!validGameObjectID)
                {
                    GetInteger($"Enter the ID of the item you wish to add to your inventory: ", 0, 0, out gameObjectID);

                    if (_gameUniverse.IsValidGameObjectByLocationId(gameObjectID, _gameTraveler.SpaceTimeLocationID))
                    {
                        Object gameObject = _gameUniverse.GetGameObjectById(gameObjectID) as Object;
                        if (gameObject.CanInventory)
                        {
                            validGameObjectID = true;
                        } else
                        {
                            ClearInputBox();
                            DisplayInputErrorMessage("It appears you may not inventory that object.  Please try again");
                        }
                    } else
                    {
                        ClearInputBox();
                        DisplayInputErrorMessage("It appears you entered an invalid game object id.  Please try again");
                    }

                }

            } else
            {
                DisplayGamePlayScreen("Pick up Game Object", "It appears there are no game objects here.", ActionMenu.ObjectMenu, "");
            }

            return gameObjectID;

        }

        public void DisplayConfirmGameObjectAddedToInventory(Object objectAddedToInventory)
        {
            if (objectAddedToInventory.PickUpMessage != null)
            {
                DisplayGamePlayScreen("Pick up game object", objectAddedToInventory.PickUpMessage, ActionMenu.ObjectMenu, "");
            }
            else
            {
                DisplayGamePlayScreen("Pick up game object", $"The {objectAddedToInventory.Name} has been added to your inventory", ActionMenu.ObjectMenu, "");
            }
        }

        /// <summary>
        /// Displaies the list of all game objects.
        /// </summary>
        public void DisplayListOfAllGameObjects()
        {
            DisplayGamePlayScreen("List: Game Objects", Text.ListAllGameObjects(_gameUniverse.GameObjects), ActionMenu.AdminMenu, "");
        }

        /// <summary>
        /// Displaies the next space time location.
        /// </summary>
        /// <returns>The next space time location.</returns>
        public int DisplayNextSpaceTimeLocation()
        {
            int spaceTimeLocationId = 0;
            bool validSpaceTimeLocationId = false;

            DisplayGamePlayScreen("Travel to a New Location", Text.Travel(_gameTraveler, _gameUniverse.SpaceTimeLocations), ActionMenu.MainMenu, "");

            while (!validSpaceTimeLocationId)
            {
                GetInteger($"Enter your new Location {_gameTraveler.Name}: ", 1, _gameUniverse.GetMaxSpaceTimeLocationId(), out spaceTimeLocationId);

                if (_gameUniverse.IsValidSpaceTimeLocationId(spaceTimeLocationId))
                {
                    if (_gameUniverse.IsAccessibleLocation(spaceTimeLocationId))
                    {
                        validSpaceTimeLocationId = true;
                    }
                    else
                    {
                        ClearInputBox();
                        DisplayInputErrorMessage("It appears you are attempting to travel to an inaccessible location.  Please try again.");
                    }
                }
                else
                {
                    DisplayInputErrorMessage("It appears you entered an invalid Location ID.  Please try again.");
                }
            }

            return spaceTimeLocationId;

        }

        /// <summary>
        /// Displaies the locations visited.
        /// </summary>
        public void DisplayLocationsVisited()
        {
            List<SpaceTimeLocation> visitedSpaceTimeLocations = new List<SpaceTimeLocation>();
            foreach (int spaceTimeLocationId in _gameTraveler.SpaceTimeLocationsVisited)
            {
                visitedSpaceTimeLocations.Add(_gameUniverse.GetSpaceTimeLocationById(spaceTimeLocationId));
            }

            DisplayGamePlayScreen("Locations Visited", Text.VisitedLocations(visitedSpaceTimeLocations), ActionMenu.TravelerMenu, "");

        }

        public void DisplayListOfAllNPCObjects()
        {
            DisplayGamePlayScreen("List: NPC Objects", Text.ListAllNPCObjects(_gameUniverse.NPCS), ActionMenu.NPCMenu, "");
        }

        #endregion

        /// <summary>
        /// displays closing screen
        /// </summary>
        /// <returns></returns>
        public bool DisplayClosingScreen()
        {
            bool playing = true;

            Console.BackgroundColor = ConsoleTheme.SplashScreenBackgroundColor;
            Console.ForegroundColor = ConsoleTheme.SplashScreenForegroundColor;
            Console.Clear();
            Console.CursorVisible = false;


            Console.SetCursorPosition(0, 10);
            string tabSpace = new String(' ', 35);
            Console.WriteLine(tabSpace + @"  _______ _           __          __       _       _                 _           ");
            Console.WriteLine(tabSpace + @" |__   __| |          \ \        / /      | |     | |               | |          ");
            Console.WriteLine(tabSpace + @"    | |  | |__   ___   \ \  /\  / /_ _ ___| |_ ___| | __ _ _ __   __| | ___ _ __ ");
            Console.WriteLine(tabSpace + @"    | |  | '_ \ / _ \   \ \/  \/ / _` / __| __/ _ \ |/ _` | '_ \ / _` |/ _ \ '__|");
            Console.WriteLine(tabSpace + @"    | |  | | | |  __/    \  /\  / (_| \__ \ ||  __/ | (_| | | | | (_| |  __/ |   ");
            Console.WriteLine(tabSpace + @"    |_|  |_| |_|\___|     \/  \/ \__,_|___/\__\___|_|\__,_|_| |_|\__,_|\___|_|   ");
            Console.WriteLine(tabSpace + @"                                                                                 ");
            Console.WriteLine(tabSpace + @"                              Thanks for playing!                                ");

            Console.SetCursorPosition(80, 25);
            Console.Write("Press any key to exit.");

            Console.ReadKey();
            playing = false;
            return playing;
        }

        #endregion
    }
}
